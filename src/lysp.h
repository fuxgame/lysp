#pragma once

#include <ctype.h>

typedef enum {
    None,
    Number,
    Decimal,
    String,
    Symbol,
    Cons,
    Subr,
    Fsubr,
    Expr,
    Fexpr,
    Psubr
} tag_t;

typedef struct Cell Cell;

typedef Cell *(*subr_t)(Cell *args, Cell *env);
typedef long 	number_t;
struct Cell {
    tag_t mTag;
    union {
        number_t	           mNumber;
        const char	           *mString;
        const char	           *mSymbol;
        struct { Cell  *a, *d; }  mCons;
        subr_t	               mSubr;
        struct { Cell *expr, *env; }		 mExpr;
    };
};

typedef void *(*alloc_t )(size_t sz);
typedef void (*free_t)(void *p);

Cell *apply(Cell *fn, Cell *args, Cell *env);

#include "gc.h"
