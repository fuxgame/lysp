#include "lsp.h"
#include "gc.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <memory.h>

#define ALIGN		sizeof(long)
#define QUANTUM		32768
#define ALLOCS_PER_GC	10000

#define VERBOSE	0


struct gc_header {
    union {
        int			flags;
        struct {
            unsigned int	used : 1;
            unsigned int	atom : 1;
            unsigned int	mark : 1;
        };
    };
    gc_header *prev;
    gc_header *next;
    size_t	    size;
} ;

static void *hdr2ptr(gc_header *hdr)	{ return (void *)(hdr + 1); }
static gc_header *ptr2hdr(void *ptr)	{ return (gc_header *)ptr - 1; }

static gc_header  gcbase = { { -1 }, &gcbase, &gcbase, 0 };
static gc_header *gcnext = &gcbase;

static int gcCount = ALLOCS_PER_GC;

void *GC_malloc(size_t lbs)
{
    gc_header *hdr, *org;
    if (!gcCount--) {
        gcCount = ALLOCS_PER_GC;
        GC_gcollect();
    }
    org = hdr = gcnext;
    lbs = (lbs + ALIGN - 1) & ~(ALIGN - 1);
#if VERBOSE > 1
    printf("malloc %d\n", lbs);
#endif
again:
#if VERBOSE > 3
    {
        gc_header *h = gcnext;
        do {
            printf("  %2d %p <- %p -> %p = %x\n", h->flags, h->prev, h, h->next, h->size);
            h = h->next;
        } while (h != gcnext);
    }
#endif
    do {
#if VERBOSE > 2
        printf("? %2d %p <- %p -> %p = %x\n", hdr->flags, hdr->prev, hdr, hdr->next, hdr->size);
#endif
        if ((!hdr->used) && (hdr->size >= lbs)) {
            void *mem;
            if (hdr->size >= lbs + sizeof(gc_header) + ALIGN) {
                gc_header *ins = (gc_header *)((gc_header *)hdr2ptr(hdr) + lbs);
                ins->flags = 0;
                ins->prev = hdr;
                ins->next = hdr->next;
                ins->size = hdr->size - lbs - sizeof(gc_header);
                hdr->next->prev = ins;
                hdr->next = ins;
                hdr->size = lbs;
            }
            hdr->used = 1;
            gcnext = hdr->next;
#if VERBOSE > 2
            printf("MALLOC %p\n", hdr);
#endif
            mem = hdr2ptr(hdr);
            memset(mem, 0, hdr->size);
            return mem;
        }
        hdr = hdr->next;
    } while (hdr != org);
    {
        size_t incr = QUANTUM;
        size_t req = sizeof(gc_header) + lbs;
        while (incr <= req) incr *= 2;
        hdr = (gc_header *)malloc(incr);
        if (hdr != (gc_header *) - 1) {
            hdr->flags = 0;
            hdr->next = &gcbase;
            hdr->prev = gcbase.prev;
            hdr->prev->next = hdr;
            gcbase.prev = hdr;
            hdr->size = incr - sizeof(gc_header);
#if VERBOSE
            printf("extend by %d at %p\n", hdr->size, hdr);
#endif
            goto again;
        }
    }
    printf("out of memory\n");
    return 0;
}

void *GC_malloc_atomic(size_t lbs)
{
    void *mem = GC_malloc(lbs);
    ptr2hdr(mem)->atom = 1;
    return mem;
}

void *GC_realloc(void *ptr, size_t lbs)
{
    gc_header *hdr = ptr2hdr(ptr);
    void *mem;
    if (lbs <= hdr->size) return ptr;
    mem = GC_malloc(lbs);
    memcpy(mem, ptr, hdr->size);
    ptr2hdr(mem)->atom = hdr->atom;
    GC_free(ptr);
    return mem;
}

gc_header *GC_freeHeader(gc_header *hdr)
{
#if VERBOSE > 1
    printf("FREE %p\n", hdr);
#endif
    hdr->flags = 0;
    if ((!hdr->prev->flags) && ((gc_header *)hdr2ptr(hdr->prev) + hdr->prev->size == hdr)) {
#if VERBOSE > 2
        printf("COALESCE PREV %p\n", hdr->prev);
#endif
        hdr->prev->next = hdr->next;
        hdr->next->prev = hdr->prev;
        hdr->prev->size += sizeof(gc_header) + hdr->size;
        hdr = hdr->prev;
    }
    if ((!hdr->next->used) && ((gc_header *)hdr2ptr(hdr) + hdr->size == hdr->next)) {
#if VERBOSE > 2
        printf("COALESCE NEXT %p\n", hdr->next);
#endif
        hdr->size += sizeof(gc_header) + hdr->next->size;
        hdr->next = hdr->next->next;
        hdr->next->prev = hdr;
    }
#if VERBOSE > 3
    {
        gc_header *h = &gcbase;
        do {
            printf("  %2d %p <- %p -> %p = %x\n", h->flags, h->prev, h, h->next, h->size);
            h = h->next;
        } while (h != &gcbase);
    }
#endif
    return hdr;
}

void GC_free(void *ptr)
{
    gcnext = GC_freeHeader(ptr2hdr(ptr));
}

void GC_default_mark_function(void *ptr)
{
    gc_header *hdr = ptr2hdr(ptr);
    void	  **pos = (void**)ptr;
    void	  **lim = (void **)hdr2ptr(hdr) + hdr->size - sizeof(void *);
    while (pos <= lim) {
        void *field = *pos;
        if (field && !((long)field & 1))
            GC_mark(field);
        ++pos;
    }
}

GC_mark_function_t GC_mark_function = GC_default_mark_function;

void GC_mark(void *ptr)
{
    gc_header *hdr = ptr2hdr(ptr);
#if VERBOSE > 2
    printf("mark? %p %d\n", hdr, hdr->flags);
#endif
    if (!hdr->mark) {
        hdr->mark = 1;
        if (!hdr->atom)
            GC_mark_function(ptr);
    }
}

GC_free_function_t GC_free_function = 0;

void GC_sweep(void)
{
    gc_header *hdr = gcbase.next;
    do {
#if VERBOSE > 2
        printf("sweep? %p %d\n", hdr, hdr->flags);
#endif
        if (hdr->flags) {
            if (hdr->mark)
                hdr->mark = 0;
            else {
                if (GC_free_function) GC_free_function(hdr2ptr(hdr));
                hdr = GC_freeHeader(hdr);
            }
        }
        hdr = hdr->next;
    } while (hdr != &gcbase);
    gcnext = gcbase.next;
}

static void ** *roots = 0;
static size_t numRoots = 0;
static size_t maxRoots = 0;

struct GC_StackRoot *GC_stack_roots = 0;

void GC_add_root(void *root)
{
    if (numRoots == maxRoots)
        roots = maxRoots
                ? (void***)realloc(roots, sizeof(roots[0]) * (maxRoots *= 2))
                : (void***)malloc (       sizeof(roots[0]) * (maxRoots = 128));
    roots[numRoots++] = (void **)root;
}

void GC_delete_root(void *root)
{
    int i;
    for (i = 0;  i < numRoots;  ++i)
        if (roots[i] == (void **)root)
            break;
    if (i < numRoots) {
        memmove(roots + i, roots + i + 1, sizeof(roots[0]) * (numRoots - i));
        --numRoots;
    }
}

void GC_gcollect(void)
{
    int i;
    struct GC_StackRoot *sr;
    for (i = 0;  i < numRoots;  ++i ) if (*roots[i])   GC_mark(*roots[i]);
    for (sr = GC_stack_roots;  sr;  sr = sr->next)	if (*(sr->root)) GC_mark(*(sr->root));
    GC_sweep();
}

size_t GC_count_objects(void)
{
    gc_header *hdr = gcbase.next;
    size_t count = 0;
    do {
        if (hdr->flags)
            ++count;
        hdr = hdr->next;
    } while (hdr != &gcbase);
    return count;
}

size_t GC_count_bytes(void)
{
    gc_header *hdr = gcbase.next;
    size_t count = 0;
    do {
        if (hdr->flags)
            count += hdr->size;
        hdr = hdr->next;
    } while (hdr != &gcbase);
    return count;
}


