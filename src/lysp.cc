#include "lysp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>

#define _balloc	GC_malloc_atomic
#define balloc	(Cell*)_balloc
#define _malloc	GC_malloc
#define malloc	(Cell*)_malloc

static void fatal( const char *fmt, ... )
{
    va_list ap;
    va_start( ap, fmt );
    fprintf( stderr, "\nError: " );
    vfprintf( stderr, fmt, ap );
    fprintf( stderr, "\n" );
    va_end( ap );
    exit( 1 );
}

Cell *mkNumber( number_t n )		{ Cell *self = balloc( sizeof( Cell ) );  self->mTag = Number;  self->mNumber = n;				return self; }
Cell *mkString( const char *s )	{ Cell *self = balloc( sizeof( Cell ) );  self->mTag = String;  self->mString = s;				return self; }
Cell *mkSymbol( const char *s )	{ Cell *self = balloc( sizeof( Cell ) );  self->mTag = Symbol;  self->mString = s;				return self; }
Cell *cons( Cell *a, Cell *d )	{ Cell *self = malloc( sizeof( Cell ) );  self->mTag = Cons;    self->mCons.a = a;     self->mCons.d = d;	return self; }
Cell *mkSubr( subr_t fn )			{ Cell *self = balloc( sizeof( Cell ) );  self->mTag = Subr;    self->mSubr = fn;				return self; }
Cell *mkFsubr( subr_t fn )		{ Cell *self = balloc( sizeof( Cell ) );  self->mTag = Fsubr;   self->mSubr = fn;				return self; }
Cell *mkExpr( Cell *x, Cell *e )	{ Cell *self = malloc( sizeof( Cell ) );  self->mTag = Expr;    self->mExpr.expr = x;  self->mExpr.env = e;	return self; }
Cell *mkFexpr( Cell *x, Cell *e )	{ Cell *self = malloc( sizeof( Cell ) );  self->mTag = Fexpr;   self->mExpr.expr = x;  self->mExpr.env = e;	return self; }
Cell *mkPsubr( subr_t fn )		{ Cell *self = balloc( sizeof( Cell ) );  self->mTag = Psubr;   self->mSubr = fn;				return self; }

#ifdef istype
#undef istype
#endif
#define istype(type)	int is##type(Cell *c) {return c && c->mTag == type;}

istype( Number )
istype( String )
istype( Symbol )
istype( Cons )
istype( Subr )
istype( Fsubr )
istype( Expr )
istype( Fexpr )
istype( Psubr )

#undef istype	// is_*
int isnil( Cell *c ) {return !c;}

#ifndef NDEBUG
# define require(X) assert(X)
#else
# define require(X) if (!(X)) return 0
#endif
/// get values
number_t	number( Cell *self )		{ require( isNumber( self ) );  return self->mNumber; }
const char *string( Cell *self )		{ require( isString( self ) );  return self->mString; }
const char *symbol( Cell *self )		{ require( isSymbol( self ) );  return self->mSymbol; }
subr_t	    subr( Cell *self )		{ require( isSubr( self ) );	return self->mSubr; }
subr_t	    fsubr( Cell *self )		{ require( isFsubr( self ) );   return self->mSubr; }
Cell	   *expr( Cell *self )		{ require( isExpr( self ) );	return self->mExpr.expr; }
Cell	   *exprenv( Cell *self )		{ require( isExpr( self ) );	return self->mExpr.env; }
Cell	   *fexpr( Cell *self )		{ require( isFexpr( self ) );   return self->mExpr.expr; }
Cell	   *fexprenv( Cell *self )	{ require( isFexpr( self ) );   return self->mExpr.env; }
subr_t	    psubr( Cell *self )		{ require( isPsubr( self ) );   return self->mSubr; }

Cell *car( Cell *self )				{ require( !self || isCons( self ) );  return self ? self->mCons.a	 : 0; }
Cell *cdr( Cell *self )				{ require( !self || isCons( self ) );  return self ? self->mCons.d	 : 0; }
Cell *rplaca( Cell *self, Cell *c )	{ require( !self || isCons( self ) );  return self ? self->mCons.a = c : c; }
Cell *rplacd( Cell *self, Cell *c )	{ require( !self || isCons( self ) );  return self ? self->mCons.d = c : c; }

#undef require

Cell *caar  ( Cell *self )		{ return car( car( self ) ); }
Cell *cadr  ( Cell *self )		{ return car( cdr( self ) ); }
Cell *cdar  ( Cell *self )		{ return cdr( car( self ) ); }
Cell *caddr ( Cell *self )		{ return car( cdr( cdr( self ) ) ); }
Cell *cadar ( Cell *self )		{ return car( cdr( car( self ) ) ); }

Cell *interns = 0;

Cell *intern( const char *s )
{
    Cell *cell = 0;
    for ( cell = interns;  cell;  cell = cdr( cell ) )
        if ( !strcmp( symbol( car( cell ) ), s ) )
            return car( cell );
    cell = mkSymbol( strdup( s ) );
    GC_PROTECT( cell );
    interns = cons( cell, interns );
    GC_UNPROTECT( cell );
    return car( interns );
}

Cell *assq( Cell *key, Cell *list )
{
    for ( ; list;  list = cdr( list ) )
        if ( key == caar( list ) ) return car( list );
    return 0;
}

Cell *print( Cell *self, FILE *stream )
{
    if ( !self ) fprintf( stream, "nil" );
    else
        switch ( self->mTag ) {
        case Number:	fprintf( stream, "%ld",        number( self ) );	      	    	break;
        case String:	fprintf( stream, "\"%s\"",     string( self ) );	      	    	break;
        case Symbol:	fprintf( stream, "%s",         symbol( self ) );	      	    	break;
        case Subr:		fprintf( stream, "subr<%p>",   subr( self ) );	      	    	break;
        case Fsubr:		fprintf( stream, "fsubr<%p>",  fsubr( self ) );	      	    	break;
        case Expr:		fprintf( stream, "(lambda " );  print( expr( self ), stream );  fprintf( stream, ")" );	break;
        case Fexpr:		fprintf( stream, "(flambda " ); print( fexpr( self ), stream ); fprintf( stream, ")" );	break;
        case Psubr:		fprintf( stream, "psubr<%p>",  psubr( self ) );			break;
        case Cons: {
            fprintf( stream, "(" );
            while ( self && isCons( self ) ) {
                print( car( self ), stream );
                if ( ( self = cdr( self ) ) ) fputc( ' ', stream );
            }
            if ( self ) {
                fprintf( stream, ". " );
                print( self, stream );
            }
            fprintf( stream, ")" );
            break;
        }
        default:
            fprintf( stream, "?%p", self );
            break;
        }
    return self;
}

Cell *println( Cell *self, FILE *stream )
{
    print( self, stream );
    fprintf( stream, "\n" );
    return self;
}

Cell *_S_t	 = 0;
Cell *_S_quote   = 0;
Cell *_S_qquote  = 0;
Cell *_S_uquote  = 0;
Cell *_S_uquotes = 0;
Cell *syntaxTable = 0;
Cell *globals = 0;

typedef Cell *( *read_t )( int, FILE * );

Cell *readFile( FILE *in );
Cell *readAlpha( int c, FILE *in );
Cell *readSign( int c, FILE *in );

read_t readers[256];

#define CEOF	((Cell *)-1)

Cell *readIllegal( int c, FILE *in )
{
    fprintf( stderr, "ignoring illegal character " );
    fprintf( stderr, ( isprint( c ) ? "%c" : "0x%02x" ), c );
    fprintf( stderr, "\n" );
    return 0;
}

Cell *readBlank( int c, FILE *in )
{
    return 0;
}

Cell *readDigit( int c, FILE *in )
{
    char buf[1024];
    int index = 0;
    char *endptr;
    number_t number = 0;
    buf[index++] = c;
    if ( '0' == c ) {
        if ( strchr( "xX", ( c = getc( in ) ) ) )	buf[index++] = c;
        else				ungetc( c, in );
    }
    while ( ( c = getc( in ) ) > 0
            && ( readDigit == readers[c] || readAlpha == readers[c] ) )
        buf[index++] = c;
    ungetc( c, in );
    buf[index] = '\0';
    errno = 0;
    number = strtol( buf, &endptr, 0 );
    if ( ( ERANGE == errno ) || ( errno && !number ) )
        perror( buf );
    if ( *endptr != '\0' )
        fprintf( stderr, "%s: invalid digits in number\n", buf );
    return mkNumber( number );
}

Cell *readAlpha( int c, FILE *in )
{
    char buf[1024];
    int index = 0;
    buf[index++] = c;
    while ( ( c = getc( in ) ) > 0
            && ( readAlpha == readers[c] || readDigit == readers[c] || readSign == readers[c] ) )
        buf[index++] = c;
    ungetc( c, in );
    buf[index] = '\0';
    return intern( buf );
}

Cell *readSign( int c, FILE *in )
{
    int d = getc( in );
    ungetc( d, in );
    return ( d > 0 && readers[d] == readDigit )
           ? readDigit( c, in ) : readAlpha( c, in );
}

Cell *readString( int d, FILE *in )
{
    char buf[1024];
    int index = 0;
    int c;
    while ( ( c = getc( in ) ) > 0 && c != d )
        if ( '\\' == ( buf[index++] = c ) )
            buf[index++] = getc( in );
    if ( c != d ) fatal( "EOF in string" );
    buf[index] = '\0';
    return mkString( strdup( buf ) );
}

Cell *readQuote( int c, FILE *in )
{
    Cell *cell = readFile( in );
    if ( CEOF == cell ) fatal( "EOF in quoted literal" );
    GC_PROTECT( cell );
    cell = cons( cell, 0 );
    cell = cons( _S_quote, cell );
    GC_UNPROTECT( cell );
    return cell;
}

Cell *readQquote( int c, FILE *in )
{
    Cell *cell = readFile( in );
    if ( CEOF == cell ) fatal( "EOF in quasiquoted literal" );
    GC_PROTECT( cell );
    cell = cons( cell, 0 );
    cell = cons( _S_qquote, cell );
    GC_UNPROTECT( cell );
    return cell;
}

Cell *readUquote( int c, FILE *in )
{
    int splice = 0;
    if ( '@' == ( c = getc( in ) ) ) splice = 1;
    else ungetc( c, in );
    Cell *cell = readFile( in );
    if ( CEOF == cell ) fatal( "EOF in quasiquoted literal" );
    GC_PROTECT( cell );
    cell = cons( cell, 0 );
    cell = cons( ( splice ? _S_uquotes : _S_uquote ), cell );
    GC_UNPROTECT( cell );
    return cell;
}

Cell *readList( int d, FILE *in )
{
    Cell *head, *tail, *cell = 0;
    tail = head = cons( 0, 0 );
    GC_PROTECT( head );
    GC_PROTECT( cell );
    switch ( d ) {
    case '(': d = ')'; break;
    case '[': d = ']'; break;
    case '{': d = '}'; break;
    }
    int c;
    for ( ;; ) {
        while ( isspace( ( c = getc( in ) ) ) );
        if ( c == d ) break;
        if ( c == ')' || c == ']' || c == '}' ) fatal( "mismatched parentheses" );
        if ( c == '.' )
            rplacd( tail, readFile( in ) );
        else {
            ungetc( c, in );
            cell = readFile( in );
            if ( feof( in ) ) fatal( "EOF in list" );
            tail = rplacd( tail, cons( cell, 0 ) );
        }
    }
    head = cdr( head );
    if ( head && isSymbol( car( head ) ) ) {
        Cell *syntax = assq( car( head ), cdr( syntaxTable ) );
        if ( syntax ) head = apply( cdr( syntax ), cdr( head ), globals );
        if ( !head ) {
            GC_UNPROTECT( head );
            return 0;
        }
    }
    GC_UNPROTECT( head );
    return head ? head : ( Cell * ) - 1;
}

Cell *readSemi( int c, FILE *in )
{
    while ( ( c = getc( in ) ) && ( c != '\n' ) && ( c != '\r' ) );
    return 0;
}

Cell *readFile( FILE *in )
{
    int c;
    Cell *cell;
    do {
        while ( isspace( c = getc( in ) ) );
        if ( c < 0 ) return ( Cell * ) - 1;
        cell = readers[c]( c, in );
    } while ( !cell );
    if ( cell == ( Cell * ) - 1 ) cell = 0;
    return cell;
}

static void initReaders( read_t r, const char *chars )
{
    while ( *chars ) readers[( int )*chars++] = r;
}

Cell *undefined( Cell *sym )
{
    fprintf( stderr, "undefined: %s\n", symbol( sym ) );
    return 0;
}

Cell *eval( Cell *expr, Cell *env );

Cell *evargs( Cell *self, Cell *env )
{
    if ( self ) {
        Cell *head, *tail;
        head = eval( car( self ), env );
        GC_PROTECT( head );
        tail = evargs( cdr( self ), env );
        GC_PROTECT( tail );
        head = cons( head, tail );
        GC_UNPROTECT( head );
        return head;
    }
    return 0;
}

Cell *evbind( Cell *expr, Cell *args, Cell *env )
{
    Cell *cell = 0;
    GC_PROTECT( env );
    GC_PROTECT( cell );
    if ( isCons( expr ) )
        for ( ;  expr;  expr = cdr( expr ), args = cdr( args ) ) {
            cell = cons( car( expr ), car( args ) );
            env = cons( cell, env );
        }
    else {
        cell = cons( expr, args );
        env = cons( cell, env );
    }
    GC_UNPROTECT( env );
    return env;
}

Cell *evlist( Cell *expr, Cell *env )
{
    Cell *result = 0;
    GC_PROTECT( expr );
    GC_PROTECT( env );
    GC_PROTECT( result );
    for ( ;  expr;  expr = cdr( expr ) ) result = eval( car( expr ), env );
    GC_UNPROTECT( expr );
    return result;
}

void *cellToPrim( Cell *cell )
{
    switch ( cell->mTag ) {
    case Cons:  case Expr:  case Fexpr:	return ( void * )cell;
    default:				return ( void * )cell->mCons.a;
    }
}

Cell *apply( Cell *fn, Cell *args, Cell *env )
{
    GC_PROTECT( fn );
    GC_PROTECT( args );
    GC_PROTECT( env );
    if ( fn )
        switch ( fn->mTag ) {
        case Subr:	return subr( fn )( evargs( args, env ), env );
        case Fsubr:	return fsubr( fn )( args, env );
        case Expr:	{
            Cell *eva = evargs( args, env );
            GC_PROTECT( eva );
            eva = evlist( cdr( expr( fn ) ), evbind( car( expr( fn ) ), eva, exprenv( fn ) ) );
            GC_UNPROTECT( fn );
            return eva;
        }
        case Fexpr:	{
            Cell *eva = cons( env, 0 );
            GC_PROTECT( eva );
            eva = cons( args, eva );
            eva = evlist( cdr( fexpr( fn ) ), evbind( car( fexpr( fn ) ), eva, fexprenv( fn ) ) );
            GC_UNPROTECT( fn );
            return eva;
        }
        case Psubr:	{ // return subr result ptr
            int i;
            void **argv;
            for ( i = 1;  args;  args = cdr( args ), ++i );
            argv = (void**)malloc( sizeof( void * )*i );

            for ( i = 1; args; args = cdr( args ), ++i ) {
                argv[i] = cellToPrim( eval( car( args ), env ) );
            }
            argv[0] = &argv[1];
            return mkNumber( ( long )psubr( args ) );
        }
        default:	break;
        }
    fprintf( stderr, "cannot apply: " );
    println( fn, stderr );
    return 0;
}

Cell *eval( Cell *expr, Cell *env )
{
    if ( !expr ) return 0;
    switch ( expr->mTag ) {
    case Number:  case String:  case Subr:  case Fsubr:  case Expr: {
        return expr;
    }
    case Symbol: {
        Cell *cell = assq( expr, env );
        if ( !cell ) return undefined( expr );
        return cdr( cell );
    }
    case Cons: {
        Cell *cell;
        GC_PROTECT( expr );
        GC_PROTECT( env );
        cell = eval( car( expr ), env );
        GC_PROTECT( cell );
        cell = apply( cell, cdr( expr ), env );
        GC_UNPROTECT( expr );
        return cell;
    }
    default:
        fatal( "unknown tag" );
    }
    return 0;
}
#ifdef EXPORT_F
#undef EXPORT_F
#endif

#define EXPORT_F(fname)	Cell *fname##_f(Cell *args, Cell *env)

EXPORT_F( define )
{
    Cell *cell = 0;
    GC_PROTECT( args );
    GC_PROTECT( env );
    if ( args ) {
        cell = cons( car( args ), 0 );
        GC_PROTECT( cell );
        rplacd( globals, cons( cell, cdr( globals ) ) );
        cell = rplacd( cell, eval( cadr( args ), env ) );
    }
    GC_UNPROTECT( args );
    return cell;
}

EXPORT_F( setq )
{
    Cell *key, *value = 0;
    GC_PROTECT( args );
    GC_PROTECT( env );
    key = car( args );
    if ( isSymbol( key ) ) {
        Cell *cell;
        value = eval( cadr( args ), env );
        GC_PROTECT( value );
        cell = assq( key, env );
        if ( !cell ) {
            GC_UNPROTECT( args );
            return undefined( key );
        }
        rplacd( cell, value );
    }
    GC_UNPROTECT( args );
    return value;
}

EXPORT_F( lambda )
{
    return mkExpr( args, env );
}

EXPORT_F( flambda )
{
    return mkFexpr( args, env );
}

EXPORT_F( let )
{
    Cell *cell, *tmp = 0;
    GC_PROTECT( args );
    GC_PROTECT( env );
    GC_PROTECT( tmp );
    for ( cell = car( args );  cell;  cell = cdr( cell ) ) {
        tmp = eval( cadar( cell ), env );
        tmp = cons( caar( cell ), tmp );
        env = cons( tmp, env );
    }
    GC_UNPROTECT( args );
    return evlist( cdr( args ), env );
}

EXPORT_F( or )
{
    Cell *value;
    GC_PROTECT( args );
    GC_PROTECT( env );
    for ( value = 0;  args && !value;  args = cdr( args ) )
        value = eval( car( args ), env );
    GC_UNPROTECT( args );
    return value;
}

EXPORT_F( and )
{
    Cell *value;
    GC_PROTECT( args );
    GC_PROTECT( env );
    for ( value = _S_t;  args && value;  args = cdr( args ) )
        value = eval( car( args ), env );
    GC_UNPROTECT( args );
    return value;
}

EXPORT_F( if )
{
    Cell *cell;
    GC_PROTECT( args );
    GC_PROTECT( env );
    cell = ( eval( car( args ), env ) ? eval( cadr ( args ), env ) : eval( caddr( args ), env ) );
    GC_UNPROTECT( args );
    return cell;
}

EXPORT_F( while )
{
    Cell *result = 0;
    GC_PROTECT( args );
    GC_PROTECT( env );
    GC_PROTECT( result );
    while ( eval( car( args ), env ) ) result = evlist( cdr( args ), env );
    GC_UNPROTECT( args );
    return result;
}

Cell *mapArgs( Cell *args )
{
    Cell *arg, *tail;
    if ( !args ) return 0;
    arg = caar( args );
    GC_PROTECT( args );
    GC_PROTECT( arg );
    rplaca( args, cdar( args ) );
    tail = mapArgs( cdr( args ) );
    arg = cons( arg, tail );
    GC_UNPROTECT( args );
    return arg;
}

EXPORT_F( map )
{
    Cell *fn = car( args ), *head, *tail, *cell = 0;
    GC_PROTECT( args );
    GC_PROTECT( env );
    GC_PROTECT( cell );
    tail = head = cons( 0, 0 );
    args = cdr( args );
    while ( car( args ) ) {
        cell = mapArgs( args );
        cell = apply( fn, cell, env );
        tail = rplacd( tail, cons( cell, 0 ) );
    }
    GC_UNPROTECT( args );
    return cdr( head );
}

EXPORT_F( eval )
{
    Cell *evalArg = car( args );
    Cell *evalEnv = cadr( args );
    GC_PROTECT( args );
    GC_PROTECT( env );
    evalArg = eval( evalArg, evalEnv ? evalEnv : env );
    GC_UNPROTECT( args );
    return evalArg;
}

EXPORT_F( apply )	{ return apply( car( args ), cdr( args ), env ); }
EXPORT_F( cons ) { return cons( car( args ), cadr( args ) ); }
EXPORT_F( rplaca )	{ return rplaca( car( args ), cadr( args ) ); }
EXPORT_F( rplacd )	{ return rplacd( car( args ), cadr( args ) ); }
EXPORT_F( car ) {return caar( args ); }
EXPORT_F( cdr ) {return cdar( args ); }
EXPORT_F( assq ) { return assq( car( args ), cadr( args ) ); }

EXPORT_F( println )
{
    for ( ;  args;  args = cdr( args ) ) {
        print( car( args ), stdout );
        if ( cdr( args ) ) putchar( ' ' );
    }
    putchar( '\n' );
    return 0;
}

//#define AROP (n,op)  n op##=
//
//#define arithmetic(name, id, op)			\
//Cell *name(Cell* args,Cell *env){								\
//	number_t n = number(car(args));			\
//	if ( cdr(args) ) {						\
//		for (args = cdr(args); args; args = cdr(args))		\ 
//			AROP(n,op) number(car(args));		\ 
//		return mkNumber(n);					\
//	}										\
//	return mkNumber(id op n);				\
//}
//
//arithmetic(add, 0, +);
//arithmetic(subtract, 0, -);
//arithmetic(multiply, 1, *);
//arithmetic(divide, 1, / );
//arithmetic(modulus, 1, %);
//
//#undef arithmetic

#define relation(name, op)					\
EXPORT_F(name)				\
{								\
  Cell *numbers;						\
  for (numbers= args;  cdr(numbers);  numbers= cdr(numbers))	\
    if (!(number(car(numbers)) op number(cadr(numbers))))	\
      return 0;							\
  return args;							\
}

relation( less,		< )
relation( lesseq,	<= )
relation( eq,		== )
relation( noteq,	!= )
relation( greatereq,	>= )
relation( greater,	> )

#undef relation

int numbersP2( Cell *args )	{ return isNumber( car( args ) ) && isNumber( cadr( args ) ); }
int numbersP3( Cell *args )	{ return isNumber( car( args ) ) && isNumber( cadr( args ) ) && isNumber( caddr( args ) ); }

Cell *primToStringSubr( Cell *args )	{ return isNumber( car( args ) ) ? mkString( strdup( ( char * )number( car( args ) ) ) ) : 0; }

typedef void *ptr;

#define access(type)																				\
  EXPORT_F(set_##type)	{ return numbersP2(args) ? mkNumber((long)((type *)number(car(args)))[number(cadr(args))]) : 0; }				\
  EXPORT_F(get_##type)	{ if (numbersP3(args)) ((type *)number(car(args)))[number(cadr(args))]= (type)number(caddr(args));  return caddr(args); }

access( char )
access( short )
access( int )
access( long )
access( ptr )

#undef access

#ifdef _WIN32
#include <windows.h>
#define dlsym	GetProcAddress
#define dlopen(filename,flag)	LoadLibrary(filename)
#define dlclose	FreeLibrary
#else
#include <dlfcn.h>
#endif
#include <conio.h>


void *rtldDefault = 0;

EXPORT_F( dlsym ) { return isString( car( args ) ) ? mkPsubr( ( subr_t )dlsym( (HMODULE)rtldDefault, string( car( args ) ) ) ) : 0; }
EXPORT_F( fsubr ) { return isPsubr ( car( args ) ) ? mkFsubr( psubr( car( args ) ) ) : 0; }
EXPORT_F( subr )	{ return isPsubr ( car( args ) ) ? mkSubr ( psubr( car( args ) ) ) : 0; }

int xFlag = 0;
int vFlag = 0;

Cell *repl( FILE *in )
{
    Cell *expr = 0, *value = 0;
    GC_PROTECT( expr );
    GC_PROTECT( value );
    while ( !feof( in ) ) {
//        if ( isatty( fileno( in ) ) ) {
            printf( "> " );
            fflush( stdout );
       // }
        expr = readFile( in );
        if ( CEOF == expr ) break;
        if ( xFlag ) println( expr, stderr );
        if ( expr ) {
            value = eval( expr, globals );
            //if ( isatty( fileno( in ) ) ) 
				println( value, stderr );
            if ( vFlag ) { fprintf( stderr, "==> " );  println( value, stderr ); }
        }
    }
    GC_UNPROTECT( expr );
    return value;
}

#if !BDWGC
void markFunction( void *ptr )
{
    Cell *cell = ( Cell * )ptr;
    assert( ptr );
    switch ( cell->mTag ) {
    case Number: case String: case Symbol: case Subr: case Fsubr:
        return;
    case Cons:
    case Expr:
    case Fexpr:
        if ( cell->mCons.a ) GC_mark( cell->mCons.a );
        if ( cell->mCons.d ) GC_mark( cell->mCons.d );
        return;
    default:
        fatal( "unknown tag" );
    }
}

void freeFunction( void *ptr )
{
    Cell *cell = ( Cell * )ptr;
    switch ( cell->mTag ) {
    case String:	free( ( void * )string( cell ) );	return;
    case Symbol:	free( ( void * )symbol( cell ) );	return;
    default:					return;
    }
}
#endif


int main( int argc, char **argv )
{
    int i;

    GC_mark_function = markFunction;
    GC_free_function = freeFunction;


    for ( i = 0;  i < 256;  ++i ) readers[i] = readIllegal;
    initReaders( readBlank,  " \t\n\v\f\r" );
    initReaders( readDigit,  "0123456789" );
    initReaders( readAlpha,  "abcdefghijklmnopqrstuvwxyz" );
    initReaders( readAlpha,  "ABCDEFGHIJKLMNOPQRSTUVWXYZ" );
    initReaders( readAlpha,  "!#$%&*/:<=>?@\\^_|~" );
    initReaders( readSign,   "+-" );
    initReaders( readString, "\"" );
    initReaders( readQuote,  "'" );
    initReaders( readQquote, "`" );
    initReaders( readUquote, "," );
    initReaders( readList,   "([{" );
    initReaders( readAlpha,  "." );
    initReaders( readSemi,   ";" );

    rtldDefault = dlopen( 0, RTLD_NOW | RTLD_GLOBAL );

    _S_t	   = intern( "t" );
    _S_quote   = intern( "quote" );
    _S_qquote  = intern( "quasiquote" );
    _S_uquote  = intern( "unquote" );
    _S_uquotes = intern( "unquote-splicing" );

    globals = cons( cons( intern( "t"	),		_S_t			 ), globals );
    globals = cons( cons( intern( "dlsym"	),	mkSubr ( dlsym_f	) ), globals );
    globals = cons( cons( intern( "fsubr" ),	mkSubr ( fsubr_f	) ), globals );
    globals = cons( cons( intern( "subr" ),		mkSubr ( subr_f 	) ), globals );
    globals = cons( cons( intern( "define" ),	mkFsubr( define_f ) ), globals );
#define T(name)		#name
#define TF(name)	name##_f
#define EXF(name)	globals = cons(cons(intern( T(name) ), mkFsubr( TF(name) ) ), globals)
#define EXS(name)	globals = cons(cons(intern( T(name) ), mkSubr( TF(name) ) ), globals)
#define EXO(b,fn)	globals = cons(cons(intern( T(b) ), mkSubr( TF(fn) ) ), globals)
    EXF( lambda );
    EXF( flambda );
    EXF( let );
    EXF( if );
    EXF( while );
    EXF( setq );

    //EXO( + , add );
    //EXO( - , subtract );
    //EXO( *, multiply );
    //EXO( / , divide  );
    //EXO( % , modulus );
    EXO( > , greater );
    EXO( < , less );
    EXO( == , eq );
    EXO( != , noteq );
    EXO( >= , greatereq );
    EXO( <= , lesseq );

    EXS( println );
    EXS( cons );
    EXS( rplaca );
    EXS( rplacd );
    EXS( car );
    EXS( cdr );
    EXS( eval );
    EXS( apply );
    EXS( map );
    EXS( assq );
#undef T
#undef EXO
#undef EXF
#undef EXS

    globals = cons( ( syntaxTable = cons( intern( "*syntax-table*" ), 0 ) ), globals );

    GC_PROTECT( globals );
    GC_PROTECT( interns );

    if ( argc == 1 ) repl( stdin );
    else {
        for ( ++argv;  argc > 1;  --argc, ++argv ) {
            if      ( !strcmp( *argv, "-v" ) ) vFlag = 1;
            else if ( !strcmp( *argv, "-x" ) ) xFlag = 1;
            else if ( !strcmp( *argv, "-" ) ) repl( stdin );
            else {
                FILE *in = fopen( *argv, "r" );
                if ( !in ) perror( *argv );
                else {
                    repl( in );
                    fclose( in );
                }
            }
        }
    }

    return 0;
}
